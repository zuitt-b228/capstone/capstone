/*
1. Customer class:

email property - string

cart property - instance of Cart class

orders property - array of objects with stucture {products: cart contents, totalAmount: cart total}

checkOut() method - pushes the Cart instance to the orders array if Cart contents is not empty


2. Product class:

name property - string

price property - number

isActive property - Boolean: defaults to true

archive() method - will set isActive to false if it is true to begin with

updatePrice() method - replaces product price with passed in numerical value


3. Cart class:

contents property - array of objects with structure: {product: instance of Product class, quantity: number}

totalAmount property - number

addToCart() method - accepts a Product instance and a quantity number as arguments, pushes an object with structure: {product: instance of Product class, quantity: number} to the contents property

showCartContents() method - logs the contents property in the console

updateProductQuantity() method - takes in a string used to find a product in the cart by name, and a new quantity. Replaces the quantity of the found product in the cart with the new value.

clearCartContents() method - empties the cart contents

computeTotal() method - iterates over every product in the cart, multiplying product price with their respective quantities. Sums up all results and sets value as totalAmount.
*/

class Customer {
	constructor(email, cart, orders){
		this.email = email;
		this.cart = new Cart;
		this.orders = [];
	}

	checkOut(){
		if(Object.keys(this.cart).length != 0){
			this.orders.push(this.cart);
		}
		return this;
	}

}

class Product {
	constructor(name, price,isActive){
		this.name = name;
		this.price = price;
		this.isActive = true;
	}

	archive(){
		if(this.isActive === "true"){
			this.isActive = false;
		}
		return this;
	}

	updatePrice(newPrice){
		if(typeof newPrice === "number"){
			this.price = newPrice;
		} else {
			return `New price must be a number`;
		}
		return this;
	}
}

class Cart {
	constructor(contents, totalAmount){
		this.contents = [];
		this.totalAmount = 0;
	}

	addToCart(productName, number){
		this.contents.push({"product": productName, "quantity": number})
		return this;
	}

	showCartContents(){
		return this.contents;
	}

	updateProductQuantity(productName, newQuantity){
		this.contents.forEach(content => {
			if(content.product.name === productName){
				content.quantity = newQuantity;
			}
		})
		return this;
	}

	clearCartContents(){
		this.contents = [];
		return this;
	}

	computeTotal(){
		let totalPerProd = 0;
		let total = 0;
		this.contents.forEach(content => {
			totalPerProd = content.product.price * content.quantity;
			total = totalPerProd + total;
		})
		this.totalAmount = total;
		return this;
	}
}

const john = new Customer("john@mail.com")
const prodA = new Product('soap', 9.99)
const prodB = new Product('food', 10.99)














